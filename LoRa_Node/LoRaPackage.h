#ifndef LORA_PACKAGE_H
#define LORA_PACKAGE_H
#include <Arduino.h>
#include <Time.h>
class LoRaPackage
{
private:
  union {
    uint16_t ival;
    byte bval[2];
  } m_source;

  union {
    uint8_t ival;
    byte bval;
  } m_messageNr;

  union {
    uint8_t ival;
    byte bval;
  } m_dataType;

  union {
    time_t tval;
    byte bval[4];
  } m_createTime;

  union {
    uint8_t i8val[8];
    uint16_t i16val[4];
    float fval[2];
    uint32_t i32val[2];
    uint64_t i64val;
    byte bval[8];
  } m_data;

public:
  LoRaPackage() {}

  LoRaPackage(uint16_t source, uint8_t messageNr, uint8_t dataType, time_t createTime)
  {
    m_source.ival = source;
    m_messageNr.ival = messageNr;
    m_dataType.ival = dataType;
    m_createTime.tval = createTime;
  }

  LoRaPackage(byte buffer[])
  {
    m_source.bval[0] = buffer[0];
    m_source.bval[1] = buffer[1];
    m_messageNr.bval = buffer[2];
    m_dataType.bval = buffer[3];
    m_createTime.bval[0] = buffer[4];
    m_createTime.bval[1] = buffer[5];
    m_createTime.bval[2] = buffer[6];
    m_createTime.bval[3] = buffer[7];
    m_data.bval[0] = buffer[8];
    m_data.bval[1] = buffer[9];
    m_data.bval[2] = buffer[10];
    m_data.bval[3] = buffer[11];
    m_data.bval[4] = buffer[12];
    m_data.bval[5] = buffer[13];
    m_data.bval[6] = buffer[14];
    m_data.bval[7] = buffer[15];
  }

  uint16_t getSource()
  {
    return m_source.ival;
  }

  void setSource(uint16_t source)
  {
    m_source.ival = source;
  }

  uint8_t getMessageNr()
  {
    return m_messageNr.ival;
  }

  void setMessageNr(uint8_t messageNr)
  {
    m_messageNr.ival = messageNr;
  }

  uint8_t getDataType()
  {
    return m_dataType.ival;
  }

  void setDataType(uint8_t dataType)
  {
    m_dataType.ival = dataType;
  }

  time_t getCreateTime()
  {
    return m_createTime.tval;
  }

  void setCreateTime(time_t createTime)
  {
    m_createTime.tval = createTime;
  }

  uint32_t getData_32(int index)
  {
    return m_data.i32val[index];
  }

  float getData_f(int index)
  {
    return m_data.fval[index];
  }

  void setData_32(uint32_t value, int index)
  {
    m_data.i32val[index] = value;
  }

  void setData_f(float value, int index)
  {
    m_data.fval[index] = value;
  }

  void toBytes(byte buffer[])
  {
    buffer[0] = m_source.bval[0];
    buffer[1] = m_source.bval[1];
    buffer[2] = m_messageNr.bval;
    buffer[3] = m_dataType.bval;
    buffer[4] = m_createTime.bval[0];
    buffer[5] = m_createTime.bval[1];
    buffer[6] = m_createTime.bval[2];
    buffer[7] = m_createTime.bval[3];
    buffer[8] = m_data.bval[0];
    buffer[9] = m_data.bval[1];
    buffer[10] = m_data.bval[2];
    buffer[11] = m_data.bval[3];
    buffer[12] = m_data.bval[4];
    buffer[13] = m_data.bval[5];
    buffer[14] = m_data.bval[6];
    buffer[15] = m_data.bval[7];
  }
};

#endif