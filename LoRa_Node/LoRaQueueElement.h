

#ifndef LORA_QUEUE_ELEMENT_H
#define LORA_QUEUE_ELEMENT_H

#include "LoRaPackage.h"
class LoRaQueueElement
{
private:
  uint8_t m_timesSend;
  time_t m_receivedTime;
  LoRaPackage *m_package;

public:
  LoRaQueueElement()
  {
    m_timesSend = 0;
  }
  LoRaQueueElement(uint8_t timesSend, time_t receivedTime, LoRaPackage *package)
  {
    m_timesSend = timesSend;
    m_receivedTime = receivedTime;
    m_package = package;
  }
  LoRaQueueElement(time_t receivedTime, LoRaPackage *package)
  {
    m_timesSend = 0;
    m_receivedTime = receivedTime;
    m_package = package;
  }

  ~LoRaQueueElement()
  {
    delete (m_package);
  }

  uint8_t getTimesSend()
  {
    return m_timesSend;
  }

  void setTimesSend(uint8_t timesSend)
  {
    m_timesSend = timesSend;
  }

  time_t getReceivedTime()
  {
    return m_receivedTime;
  }

  void setReceivedTime(time_t receivedTime)
  {
    m_receivedTime = receivedTime;
  }

  LoRaPackage *getPackage()
  {
    return m_package;
  }

  void setPackage(LoRaPackage *package)
  {
    m_package = package;
  }
};

#endif