#include "LoRaPackage.h"
#include "LoRaQueueElement.h"
#include <LinkedList.h> //see: https://github.com/ivanseidel/LinkedList
#include <LoRa.h>
#include <SPI.h>
#include <SoftwareSerial.h>
#include <Time.h>
#include <TimeLib.h>
#include <TinyGPS++.h>

/*
The Algorithem to calculate the new delayTime is:
Bytes per Hour Out (BPH_OUT) = (MAX_BPH - max(<Bytes in last Hour>,<1% of MAX_BPH>))
Delay in Seconds (delayTime)= 3600 /(BPH_OUT*MAX_PACKAGES_PER_INTERVAL*PACK_SIZE)
*/
#define MAX_BPH 11200               //Max Byte per Hour IN and OUT
#define RECALC_PERIOD 3600          // How often will the intervall duration be recalculated
#define MAX_PACKAGES_PER_INTERVAL 5 //Max nummber of packages send per intervall
#define OWN_PACKAGES 1              //How many of MAX_PACKAGES_PER_INTERVAL are my own Packages
#define MAX_AGE 120                 //How old is a sensor reading allowed to be before it is filtered out
#define PACK_SIZE 16                //How Big is a Package
#define MAX_REPEAT 3

#define ADDRESS 3  //My Own Address, shoueld be unique

#define DATA_TYPE 1 //Datatype of my Sensors, 1=GPS
#define RX 3        //RX PIN for GPS
#define TX 2        //TX PIN for GPS

SoftwareSerial nss(RX, TX); //Softwar Serial to read GPS Data (See Jumper Cables)
TinyGPSPlus gps;

uint32_t delayTime = 30;      //in seconds, is recalculated automatically see: recalculateDelay();
uint32_t receivedBytes = 0;   //Bytes in last recalculation period. A period is determined by delayTime
time_t lastSend = 0;          //when were the last send period. unix timestamp of when the last sending happend
time_t delayRecalculated = 0; // when has the last recalculation happend, unic timestamp
uint8_t msgNr = 0;            //the messageNr used for own  Packages. Is increased with each Packages send.
enum dataType
{
  UNKNOWN,
  GPS,
  TEMP,
}; //max 255 verschiedene Typen

// The Queue for all PAckages in Cache.
LinkedList<LoRaQueueElement *> *queue = new LinkedList<LoRaQueueElement *>();

void setup()
{
  //begin GPS serial stream
  nss.begin(9600);
  //begin normal serialsream (COM)
  Serial.begin(9600);

  // Serial.println("LoRa Node");

  //Start LoRa
  if (!LoRa.begin(868E6))
  {
    if (Serial)
      Serial.println("Starting LoRa failed!");
    while (1)
      ;
  }
  if (Serial)
    Serial.println("LoRa Node [" + String(ADDRESS) + "]");
  //Setup of Sensors would be here

  //callback for receiving of packages
  LoRa.onReceive(onReceive);
  LoRa.receive();
}

//if a LoRa PAckage is received
void onReceive(int packetSize)
{
  //add to bytes received statistics
  receivedBytes += packetSize;
  //Package must be of the size of a LoRaPackage (see: LoRaPackage.h)
  if (packetSize == PACK_SIZE)
  {
    // create buffer
    byte dataBuffer[PACK_SIZE];

    // read packet
    for (int i = 0; i < PACK_SIZE; i++)
    {
      dataBuffer[i] = LoRa.read();
    }
    //create LoRaPAckage Object
    LoRaPackage *newPack = new LoRaPackage(dataBuffer);

    //Ignore Package if its from own address or too old
    if (newPack->getSource() == ADDRESS || newPack->getCreateTime() + MAX_AGE < now())
    {
      return;
    }

    //check if package is allready in queue and remove old packages
    bool isOld = false;
    for (int i = 0; i < queue->size(); i++)
    {
      if (queue->get(i)->getPackage()->getSource() == newPack->getSource())
      {
        if (queue->get(i)->getPackage()->getCreateTime() > newPack->getCreateTime())
        {
          //Package is older than Package in Queue
          isOld = true;
        }
        else
        {
          //Package in Queue id older
          delete (queue->get(i));
          queue->remove(i--);
        }
      }
    }

    //add package to queue if it is not too old
    if (!isOld)
    {
      queue->add(new LoRaQueueElement(now(), newPack));
    }
  }
}

void sendData()
{
  LoRa.idle();
  //get own sensor data, create Packages and send them
  //GPS
  LoRaPackage *myPack = new LoRaPackage();
  readGpsData(myPack);
  sendLoRaPackage(myPack);
  printLoRaPackage(myPack);
  //More own sensordata...
  LoRa.receive();
}

void sendDataExternal()
{
  LoRa.idle();

  //remove old packages
  for (int i = 0; i < queue->size(); i++)
  {
    LoRaPackage *p = queue->get(i)->getPackage();
    if (p->getCreateTime() + MAX_AGE < now() || queue->get(i)->getTimesSend() >= MAX_REPEAT)
    {
      delete (p);
      delete (queue->get(i));
      queue->remove(i);
    }
  }

  // send packages from other nodes. (Chached Packages)
  queue->sort(compare);
  for (int i = 0; i < queue->size(); i++)
  {
    LoRaPackage *p = queue->get(i)->getPackage();
    //Limit Packages send to MAX_PACKAGES_PER_INTERVAL
    if (i < (MAX_PACKAGES_PER_INTERVAL - OWN_PACKAGES))
    {
      sendLoRaPackage(p);
      queue->get(i)->setTimesSend(queue->get(i)->getTimesSend() + 1);
    }
    //Print all PAckages to COM
    printLoRaPackage(p);
  }

  LoRa.receive();
}

bool initial = true;
void loop()
{
  //GPS Data available
  if (nss.available() > 0)
  { //encode GPS Data
    gps.encode(nss.read());
    //If GPS contains Time -> set internal Time
    if (gps.time.isValid() && gps.date.isValid() && gps.time.isUpdated() && gps.date.isUpdated() && gps.time.age() < 500 && gps.date.age() < 500)
    {
      setTime(gps.time.hour(), gps.time.minute(), gps.time.second(), gps.date.day(), gps.date.month(), gps.date.year());
    }
  }

  //if Time has been initialized
  if (timeStatus() != timeNotSet)
  {
    if (initial)
    {
      if (Serial)
        Serial.println("Time Set");
      delayRecalculated = now();
      lastSend = now();
      initial = false;
    }
    //recalculation delay is over
    if (delayRecalculated + RECALC_PERIOD < now())
    {
      //recalculate delay
      recalculateDelay();
      delayRecalculated = now();
    } //send delay is over
    if (lastSend + delayTime < now())
    {
      //send data

      if (Serial)
      {
        Serial.println("Sending data...");
        Serial.println("OWN:");
      }
      sendData();

      if (Serial)
        Serial.println("REMOTE:");
      sendDataExternal();
      lastSend = now();
      if (Serial)
        Serial.println();
    }
  }
  else
  {
    //send delay is over
    if (lastSend + delayTime < now())
    {
      //send data

      if (Serial)
      {
        Serial.println("Sending data...");
        Serial.println("OWN:");
        Serial.println("REMOTE:");
      }
      sendDataExternal();
      lastSend = now();

      if (Serial)
        Serial.println();
    }
  }
}
//get GPS Data and put it into a LoRaPackage
void readGpsData(LoRaPackage *p)
{
  p->setSource(ADDRESS);
  p->setDataType(DATA_TYPE);
  p->setMessageNr(++msgNr);
  p->setCreateTime(now());
  p->setData_f(gps.location.lng(), 0);
  p->setData_f(gps.location.lat(), 1);
}

void sendLoRaPackage(LoRaPackage *p)
{
  //transform LoRaPackage into byte buffer
  byte buffer[16];
  p->toBytes(buffer);
  //write Byte Buffer
  LoRa.beginPacket();
  LoRa.write(buffer, PACK_SIZE);
  LoRa.endPacket();
}

//Print LoRaPackage to COM as JSON
void printLoRaPackage(LoRaPackage *p)
{
  if (Serial)
    Serial.print("{\"source\": " + String(p->getSource()) + ",\"message_nr\": " + String(p->getMessageNr()) + ",\"create_time\": \"" + timeToString(p->getCreateTime()) + "\"," + dataToString(p) + "}\r\n");
}

//Create UTC Time String (No Timezone included)
String timeToString(time_t t)
{
  return String(year(t)) + "-" + String(month(t)) + "-" + String(day(t)) + ", " + String(hour(t)) + ":" + String(minute(t)) + ":" + String(second(t));
}

//Turn Data into String
String dataToString(LoRaPackage *p)
{
  if (p->getDataType() == GPS)
  {
    return "\"lat\": " + String(p->getData_f(0), 7) + ",\"long\": " + String(p->getData_f(1), 7);
  }
  else if (p->getDataType() == TEMP)
  {
    return "\"temp\": " + String(p->getData_32(0));
  }
  return "\"data\":\"unknown\"";
}

//Compare two LoRaQueueElement Object,
//first order them by how manny times they were send, then by when they were received
int compare(LoRaQueueElement *&e1, LoRaQueueElement *&e2)
{
  if (e1->getTimesSend() != e2->getTimesSend())
  {
    return e1->getTimesSend() - e2->getTimesSend();
  }
  return e1->getReceivedTime() - e2->getReceivedTime();
}

//Recalculate delay, see formula at the top
void recalculateDelay()
{
  double minBytesPerHour = MAX_BPH / 100.0;                                                   //1% Rule
  double receivedBytesPerHour = ((double)receivedBytes) * (3600.0 / ((double)RECALC_PERIOD)); //if recalculation is not one hour scale the counted bytes accordingly
  double bph_out = ((double)MAX_BPH) - max(receivedBytesPerHour, minBytesPerHour);
  delayTime = 3600 / (bph_out / (MAX_PACKAGES_PER_INTERVAL * PACK_SIZE));
  //reset count
  if (Serial)
    Serial.println("new Delay: " + String(delayTime) + " received bytes: " + String(receivedBytes));
  receivedBytes = 0;
}
