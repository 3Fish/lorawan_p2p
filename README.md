## Relevante Sites
* [TinyGPS++ Libary](http://arduiniana.org/libraries/tinygpsplus/)
* [LinkedList Libary](https://github.com/ivanseidel/LinkedList)
* [Arduino LoRa Library](https://github.com/sandeepmistry/arduino-LoRa/)
* [SoftwareSerial Library](https://www.arduino.cc/en/Reference/SoftwareSerial)
* [Build a LoRa Gateway Blogpost](https://www.instructables.com/id/Use-Lora-Shield-and-RPi-to-Build-a-LoRaWAN-Gateway/)
* [Dragino LoRa Shield (Pin Belegung)](http://wiki.dragino.com/index.php?title=Lora_Shield#Example1_--_Use_with_LMIC_library_for_LoraWAN_compatible)
* [Dragino LoRa Shield Wiki](http://wiki.dragino.com/index.php?title=Connect_to_TTN#Use_LoRa_GPS_Shield_and_Arduino_as_LoRa_End_Device)
* [LoRa P2P Blogpost](https://hackaday.io/project/162667-lora-neural-network-security-system/log/156707-getting-lora-peer-to-peer-working)
* [Time Library](https://github.com/PaulStoffregen/Time)
* [GPS GPRMC & GPGGA Converter](https://rl.se/gprmc)

