#include <TinyGPS++.h>
#include <SoftwareSerial.h>

#define TXPIN 3
#define RXPIN 2
SoftwareSerial nss(TXPIN, RXPIN);
TinyGPSPlus gps;

void setup() {
  Serial.begin(9600); // opens serial port, sets data rate to 9600 bps
  nss.begin(9600);
  while (!Serial);
  Serial.println("GPS Test");
}


void loop()
{
  while (nss.available() > 0)
  {
    Serial.println("Reading Data");
    int c = nss.read();
    if (gps.encode(c))
    {
      if (gps.location.isValid() && gps.location.isUpdated())
        Serial.println("Lat: " + String(gps.location.lat()) + " Lon: " + String(gps.location.lng()));
      if (gps.altitude.isValid() && gps.altitude.isUpdated())
        Serial.println("Alt: " + String(gps.altitude.meters()));
      if (gps.time.isValid() && gps.time.isUpdated())
        Serial.println("Time: " + String(gps.time.hour()) + ":" + String(gps.time.minute()) + ":" + String(gps.time.second()));
      if (gps.date.isValid() && gps.date.isUpdated())
        Serial.println("Date: " + String(gps.date.day()) + "." + String(gps.date.month()) + "." + String(gps.date.year()));
      if (gps.speed.isValid() && gps.speed.isUpdated())
        Serial.println("Speed: " + String(gps.speed.kmph()) + " Km/h");
      if (gps.course.isValid() && gps.course.isUpdated())
        Serial.println("Course: " + String(gps.course.deg()) + "°");
      if (gps.satellites.isValid() && gps.satellites.isUpdated())
        Serial.println("Satelites: " + String(gps.satellites.value()));
      if (gps.hdop.isValid() && gps.hdop.isUpdated())
        Serial.println("HDop: " + String(gps.hdop.value()));
    }
  }
}
