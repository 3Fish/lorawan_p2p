#include <Time.h>
#include <TimeLib.h>

#include <SPI.h>
#include <LoRa.h>

void setup() {
  Serial.begin(9600);
  while (!Serial);

  if (!LoRa.begin(868E6)) {
    Serial.println("Starting LoRa failed!");
    while (1);
  }

  
  Serial.println("LoRa Receiver");
}

union {
  float fval;
  byte bval[4];
} f2b1,f2b2;

union {
  time_t tval;
  byte bval[4];
} t2b;

void loop() {
  // try to parse packet
  int packetSize = LoRa.parsePacket();
  if (packetSize == 12) {
    
    byte buffer[12];
    // received a packet
   
    for (int i = 0; LoRa.available() && i < 12; i++) {
      buffer[i] = LoRa.read();
    }
    time_t timeStamp = 0;
    float lat = 0, lon = 0;
    // read packet
    t2b.bval[0] = buffer[0];
    t2b.bval[1] = buffer[1];
    t2b.bval[2] = buffer[2];
    t2b.bval[3] = buffer[3];
    timeStamp = t2b.tval;
    f2b1.bval[0] = buffer[4];
    f2b1.bval[1] = buffer[5];
    f2b1.bval[2] = buffer[6];
    f2b1.bval[3] = buffer[7];
    lat = f2b1.fval;

    f2b2.bval[0] = buffer[8];
    f2b2.bval[1] = buffer[9];
    f2b2.bval[2] = buffer[10];
    f2b2.bval[3] = buffer[11];
    lon = f2b2.fval;
    
    Serial.println("time:"+String(hour(timeStamp))+":"+String(minute(timeStamp))+":"+String(second(timeStamp))+" date:"+String(day(timeStamp))+"."+String(month(timeStamp))+"."+String(year(timeStamp)) + " lat:" + String(lat,7) + " lon:" + String(lon,7)+" rssi:"+LoRa.packetRssi()+" snr:"+LoRa.packetSnr());
  }
}
