#include <Time.h>
#include <TimeLib.h>

#include <SoftwareSerial.h>
#include <LoRa.h>
#include <TinyGPS++.h>
#define RXPIN 3
#define TXPIN 2
SoftwareSerial nss(RXPIN, TXPIN);
TinyGPSPlus gps;



time_t lastSend;
bool waitForDelay, noGPSData;

void setup() {
  Serial.begin(9600); // opens serial port, sets data rate to 9600 bps
  nss.begin(9600);
  while (!nss);
  while (!Serial);
  if (!LoRa.begin(868E6)) {
    Serial.println("Starting LoRa failed!");
    while (1);
  }
  Serial.println("GPS Beacon");
  lastSend = 0;
  waitForDelay = false;
  noGPSData = false;
}

union {
  float fval;
  byte bval[4];
} f2B1, f2B2;

union {
  time_t tval;
  byte bval[4];
} t2B;

void loop()
{
  while (nss.available())
  {
    int c = nss.read();
    if (gps.encode(c))
    {
      if (gps.time.isValid() && gps.time.isUpdated() && gps.time.age() < 500 & gps.date.isValid() && gps.date.isUpdated() && gps.date.age() < 500)
        setTime(gps.time.hour(), gps.time.minute(), gps.time.second(), gps.date.day(), gps.date.month(), gps.date.year());

      if (lastSend + 30 < now()) {
        waitForDelay = false;
        t2B.tval = now();
        if (gps.location.isValid() && gps.location.isUpdated()) {
          noGPSData=false;
          f2B1.fval = gps.location.lat();
          f2B2.fval = gps.location.lng();

          Serial.println("New Data:  " + String(t2B.tval) + " -> " + String(f2B1.fval) + "," + String(f2B2.fval));
          // send packet
          if (LoRa.beginPacket()) {
            Serial.println("Send Data: " + String(t2B.tval, HEX) + String(f2B1.fval, HEX) + String(f2B2.fval, HEX));
            LoRa.write(t2B.bval, 4);
            LoRa.write(f2B1.bval, 4);
            LoRa.write(f2B2.bval, 4);
            LoRa.endPacket();
            lastSend = now();
            Serial.println("Data send");
          }
          else {
            Serial.println("Sender not ready");
          }
        } else if (!noGPSData) {
          Serial.println("No new GPS data available");
          noGPSData = true;
        }
      } else if (!waitForDelay) {

        Serial.println("delay time is not up");
        waitForDelay = true;
      }
    }
  }
}
