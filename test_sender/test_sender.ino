#include <SPI.h>
#include <LoRa.h>


void setup() {
  Serial.begin(9600);
  while (!Serial);

  Serial.println("LoRa Sender");

  if (!LoRa.begin(868E6)) {
    Serial.println("Starting LoRa failed!");
    while (1);
  }
}

void loop() {

  if (Serial.available()){
    String msg = Serial.readString();

    byte data[msg.length()];
    msg.getBytes(data, msg.length());
   
  
    // send packet
    LoRa.beginPacket();
    LoRa.write(data,msg.length());
    LoRa.endPacket();
  
}
